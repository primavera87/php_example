<?php
    include "lib.inc.php";
	include "data.inc.php";

	setlocale(LC_ALL, "ru_RU.UTF8");
	
//Инициализация заголовков страницы
$title = 'Сайт нашей школы';
$header = "$welcome, гость!";
$id = strtolower(strip_tags(trim($_GET['id'])));

switch($id){
	case 'about':
	    $title = 'О сайте';
		$header = 'О нашем сайте';
	break;
	case 'contact':
	    $title = 'Контакты';
		$header = 'Обратная связь';
	break;
	case 'table':
	    $title = 'Таблица умножения';
		$header = 'Таблица умножения';
	break;
	case 'calc':
	    $title = 'Он-лайн калькулятор';
		$header = 'Калькулятор';
	break;
}
?>
<!DOCTYPE html>
<html>

<head>
  <title><?php echo $title?></title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="style.css" />
</head>

<body>

  <div id="header">
    <?php include "top.inc.php"?>
  </div>

  <div id="content">
    <!-- Заголовок -->
    <h1><?php echo "$welcome, гость!";?></h1>
	<h1><?php echo $header?></h1>
    <!-- Заголовок -->
<blockquote>
<?php
 $day = strftime('%d');
 $mon = strftime('%B');
 $year = strftime('%Y');
	echo "Сегодня $day число, $mon месяц, $year год.";
?>
</blockquote>
<nav>
   <?php
     drawMenu( [['link'=>'Специалист', 'href'=>'http://www.specialist.ru'],[
	 'link'=>'Яндекс', 'href'=>'http://www.yandex.ru']], false);
   ?>
</nav>
    <!-- Область основного контента -->

	<?php
	switch($id){
		case 'about': include 'about.php'; break;
		case 'contact': include 'contact.php'; break;
		case 'table': include 'table.php'; break;
		case 'calc': include 'calc.php'; break;
		default: include 'index.inc.php';
	}
	?>
	
    <!-- Область основного контента -->
  </div>
  <div id="nav">
  
    <?php include "menu.inc.php"?>

  </div>
  <div id="footer">
    <?php include "bottom.inc.php"?> 
  </div>
</body>

</html>