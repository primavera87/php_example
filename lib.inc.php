<?php
function drawTable($cols, $rows, $color) {

	  echo "<table border='1' width='200'>";
	    for($i = 1; $i <= $rows; $i++) {
             echo "<tr>";
             for($j = 1; $j <=$cols; $j++) {
              if ( ($i == 1) || ($j == 1) )
				echo "<td style='background:$color'>";  
			   else
			     echo "<td>";
			   echo $i*$j, "</td>";
             }
             echo "</tr>";
		}
        echo "</table>";	

}

function drawMenu($menu, bool $vertical) {
	if ($vertical) {
     echo '<ul>';
	foreach($menu as $item)
	    echo "<li><a href='",$item['href'],"'>",$item[
		 'link'], "</a></li>";
    echo '</ul>';	
	}
	else 
		foreach($menu as $item)
	     echo "<a href='",$item['href'],"'>",$item[
		 'link'], "</a>&nbsp;";
}	
?>