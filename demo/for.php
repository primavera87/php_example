<!DOCTYPE html>
<html>
<head>
  <title>Циклы</title>
</head>
<body>
<h1>Циклы</h1>
<?php
   
   for($i = 1; $i <= 50; $i+=2) {
	   echo $i;
	   echo '<br>';
   }
   
   echo '<hr>';
   for($i = 100; $i > 0; $i-=3) {
	   echo $i;
	   echo '<br>';
   }
   
?>
</body>
</html>