<?php
   if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
       $num1 = (int)$_POST['num1'];
       $num2 = (int)$_POST['num2'];
       $op = trim(strip_tags($_POST['operator']));

       if (!(strval($num1) == $_POST['num1'] && strval($num2) == $_POST['num2']))
           $result = "Неверное число";
           else
           {
             switch($op)
             {
               case '+' : $r = $num1+$num2; break;
               case '-' : $r = $num1-$num2; break;
               case '*' : $r = $num1*$num2; break;
               case '/' : 
                   if ($num2 == 0)
                   $result = "Деление на ноль";
                   else {
                     $r = $num1/$num2;
                   }
                
             }
           }
    }
?>
<form action='<?= $_SERVER['REQUEST_URI']?>' method="POST">
    <label>Число 1:</label><br />
    <input name='num1' type='number' value='<?=$num1?>'/><br />
    <label>Оператор: </label><br />
    <select name='operator'>
        <option <?= ($op=='+') ? 'selected':''?>>+</option>
        <option <?= ($op=='-') ? 'selected':''?>>-</option>
        <option <?= ($op=='*') ? 'selected':''?>>*</option>
        <option <?= ($op=='/') ? 'selected':''?>>/</option>
    </select>
    <br />
    <label>Число 2: </label><br />
    <input name='num2' type='number' value='<?=$num2?>'/><br />
    <label>= <?=$r?> </label><br />

    <input type='submit' value='Считать'>
    
</form>