<?php
$leftMenu = [
    ['link'=>'Домой', 'href'=>'index.php'], 
	['link'=>'О нас', 'href'=>'index.php?id=about'], 
	['link'=>'Контакты', 'href'=>'index.php?id=contact'], 
	['link'=>'Таблица умножения', 'href'=>'index.php?id=table']
];
$leftMenu[] = ['link'=>'Калькулятор', 'href'=>'index.php?id=calc'];


  //Установка локали и выбор значений даты
  setlocale(LC_ALL, "russian");
//   $day = strftime('%d');
//   $mon = strftime('%B');
//   $year = strftime('%Y');
define('COPYRIGHT', 'Супер Мега Веб-мастер', true);

$hour = (int)strftime('%H');
$welcome = '';

if ($hour >=0 && $hour < 6)
	$welcome = 'Доброй ночи';
elseif ($hour >=6 && $hour < 12)
	$welcome = 'Доброе утро';
elseif ($hour >=12 && $hour < 18)
	$welcome = 'Добрый день';
elseif ($hour >=18 && $hour < 23)
	$welcome = 'Доброй вечер';
?>